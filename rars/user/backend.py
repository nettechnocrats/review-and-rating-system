from .models import User

class LoginBackend:

    def authenticate(self, request, email = None, **kwargs):
        try:
        	user = User.objects.get(email = request)
        except User.DoesNotExist:
        	return None
    	
        return user
        
    def get_user(self, user_id):
        
        user = User.objects.get(user_id = user_id)
        
        return user