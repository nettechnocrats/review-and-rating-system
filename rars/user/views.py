from django.shortcuts import render

# Create your views here.
from django.shortcuts import render,redirect
import requests
import json
from .models import User
from django.contrib.auth import authenticate, login,logout
from .backend import LoginBackend
# Create your views here.

def home(request):
	user_id = request.session.get('user_id',None)
	if user_id:
		return redirect('/dashboard/')

	return render(request, 'index.html')

def signin(request):
	return render(request, 'accounts/sign-in.html')

def signout(request):

	request.session.pop('user_id', None)
	logout(request)
	
	return redirect('/')


def signin_redirect(request):
	
	code = request.GET.get('code',None)

	access_token_url = "https://www.linkedin.com/oauth/v2/accessToken"

	payload = "grant_type=authorization_code&code="+code+"&redirect_uri=http%3A%2F%2F127.0.0.1%3A8000%2Fuser%2Fredirect%2F&client_id=81e1glrv8wu6lj&client_secret=yN6zVbQPlUvdT4W2"
	headers = {
			    'content-type': "application/x-www-form-urlencoded",
			    'cache-control': "no-cache"
    }

	response 	 = requests.request("POST", access_token_url, data=payload, headers=headers)
	response 	 = response._content.decode('utf-8')
	response 	 = json.loads(response)
	access_token = response['access_token']
	profile_url  = "https://api.linkedin.com/v2/me"

	headers 	 = {
					'Authorization': "Bearer "+access_token,
    				'content-type': "application/x-www-form-urlencoded",
    				'X-Restli-Protocol-Version': '2.0.0'
    				}
	resp 		 = requests.request("GET", profile_url, headers=headers)
	resp 		 = resp._content.decode('utf-8')
	resp 		 = json.loads(resp)
	
	email_url  	 = 'https://api.linkedin.com/v2/emailAddress?q=members&projection=(elements*(handle~))'

	email_resp	 = requests.request("GET", email_url, headers=headers)
	email_resp   = email_resp._content.decode('utf-8')
	email_resp   = json.loads(email_resp)
	email 		 = email_resp['elements'][0]['handle~']['emailAddress']

	try:
		user = User.objects.get(email = email)
	except:
		user = User.objects.create(email = email, first_name = resp['localizedFirstName'], 
									   last_name = resp['localizedLastName'], linkedin_id = resp['id'])

	user_obj = LoginBackend.authenticate(request, email)
	if user_obj is not None:
		text = login(request, user_obj)

	request.session['user_id'] = user.id

	return redirect('/dashboard/')