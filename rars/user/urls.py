from django.urls import path
from .views import signin, signout, signin_redirect

urlpatterns = [
    path('', signin ,name ='signin' ),
    path('signout/', signout ,name ='signout' ),
    path('redirect/', signin_redirect ,name ='signin_redirect' )
]