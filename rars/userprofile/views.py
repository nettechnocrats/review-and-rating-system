from django.shortcuts import render

# Create your views here.
def user_profile_screen(request):
	return render(request, 'user-profile/edit-profile.html')